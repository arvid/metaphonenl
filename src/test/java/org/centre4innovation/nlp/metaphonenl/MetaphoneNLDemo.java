package org.centre4innovation.nlp.metaphonenl;

/**
 * @author Arvid Halma
 * @version 5-9-18
 */
public class MetaphoneNLDemo {
    public static void main(String[] args) {
        MetaphoneNL metaphoneNL = new MetaphoneNL();
        System.out.println("metaphoneNL.normalize(\"vrede\") = " + metaphoneNL.normalize("vrede"));
        System.out.println("metaphoneNL.normalize(\"wrede\") = " + metaphoneNL.normalize("wrede"));
        System.out.println("metaphoneNL.normalize(\"china\") = " + metaphoneNL.normalize("china"));
        System.out.println("metaphoneNL.normalize(\"Ik heet Arvid en ik zou je graag welkom heten bij deze metaphone normalizer!\") \n  = " + metaphoneNL.normalize("Ik heet Arvid en ik zou je graag welkom heten bij deze metaphone normalizer!".toLowerCase()));
    }
}
